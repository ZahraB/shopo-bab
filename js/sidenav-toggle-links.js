//Menu button in topnav
function openMenu() {
    $(".dropdown-menu").toggle();
}

$(document).ready(function() {
    //Mode toggle
    $("#plus").click(function() {
        $(".toggle").slideDown()
        $("#plus").hide()
        $("#minus").show()
        if ($(".toggle1,.toggle2,.toggle3,.toggle4").slideDown()) {
            $(".toggle1,.toggle2,.toggle3,.toggle4").hide()
            $("#minus1,#minus2,#minus3,#minus4").hide()
            $("#plus1,#plus2,#plus3,#plus4").show()
        }
    })
    $("#minus").click(function() {
        $(".toggle").slideUp()
        $("#minus").hide()
        $("#plus").show()
    })
    //Electronic toggle
    $("#plus1").click(function() {
        $(".toggle1").slideDown()
        $("#plus1").hide()
        $("#minus1").show()
        if ($(".toggle,.toggle2,.toggle3,.toggle4").slideDown()) {
            $(".toggle,.toggle2,.toggle3,.toggle4").hide()
            $("#minus,#minus2,#minus3,#minus4").hide()
            $("#plus,#plus2,#plus3,#plus4").show()
        }
    })
    $("#minus1").click(function() {
        $(".toggle1").slideUp()
        $("#minus1").hide()
        $("#plus1").show()
    })
    //Home & garden toggle
    $("#plus2").click(function() {
        $(".toggle2").slideDown()
        $("#plus2").hide()
        $("#minus2").show()
        if ($(".toggle,.toggle1,.toggle3,.toggle4").slideDown()) {
            $(".toggle,.toggle1,.toggle3,.toggle4").hide()
            $("#minus,#minus1,#minus3,#minus4").hide()
            $("#plus,#plus1,#plus3,#plus4").show()
        }
    })
    $("#minus2").click(function() {
        $(".toggle2").slideUp()
        $("#minus2").hide()
        $("#plus2").show()
    })
    //Music toggle
    $("#plus3").click(function() {
        $(".toggle3").slideDown()
        $("#plus3").hide()
        $("#minus3").show()
        if ($(".toggle,.toggle1,.toggle2,.toggle4").slideDown()) {
            $(".toggle,.toggle1,.toggle2,.toggle4").hide()
            $("#minus,#minus1,#minus2,#minus4").hide()
            $("#plus,#plus1,#plus2,#plus4").show()
        }
    })
    $("#minus3").click(function() {
        $(".toggle3").slideUp()
        $("#minus3").hide()
        $("#plus3").show()
    })
    //Pages toggle
    $("#plus4").click(function() {
        $(".toggle4").slideDown()
        $("#plus4").hide()
        $("#minus4").show()
        if ($(".toggle,.toggle1,.toggle2,.toggle3").slideDown()) {
            $(".toggle,.toggle1,.toggle2,.toggle3").hide()
            $("#minus,#minus1,#minus2,#minus3").hide()
            $("#plus,#plus1,#plus2,#plus3").show()
        }
    })
    $("#minus4").click(function() {
        $(".toggle4").slideUp()
        $("#minus4").hide()
        $("#plus4").show()
    })
})