$(document).ready(function() {
    //Mode toggle
    $("#minus-toggle").hide()
    $("#plus-toggle").click(function() {
        $(".sidenav-toggle").show()
        $("#plus-toggle").hide()
        $("#minus-toggle").show()
        if ($(".sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle3,.sidenav-toggle4").show()) {
            $(".sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle3,.sidenav-toggle4").hide()
            $("#minus-toggle1,#minus-toggle2,#minus-toggle3,#minus-toggle4").hide()
            $("#plus-toggle1,#plus-toggle2,#plus-toggle3,#plus-toggle4").show()
        }
    })
    $("#minus-toggle").click(function(){
        $(".sidenav-toggle").hide()
        $("#minus-toggle").hide()
        $("#plus-toggle").show()
    })

    //Electronic toggle
    $("#minus-toggle1").hide()
    $("#plus-toggle1").click(function() {
        $(".sidenav-toggle1").show()
        $("#plus-toggle1").hide()
        $("#minus-toggle1").show()
        if ($(".sidenav-toggle,.sidenav-toggle2,.sidenav-toggle3,.sidenav-toggle4").show()) {
            $(".sidenav-toggle,.sidenav-toggle2,.sidenav-toggle3,.sidenav-toggle4").hide()
            $("#minus-toggle,#minus-toggle2,#minus-toggle3,#minus-toggle4").hide()
            $("#plus-toggle,#plus-toggle2,#plus-toggle3,#plus-toggle4").show()
        }
    })
    $("#minus-toggle1").click(function(){
        $(".sidenav-toggle1").hide()
        $("#minus-toggle1").hide()
        $("#plus-toggle1").show()
    })

    //Home & garden toggle
    $("#minus-toggle2").hide()
    $("#plus-toggle2").click(function() {
        $(".sidenav-toggle2").show()
        $("#plus-toggle2").hide()
        $("#minus-toggle2").show()
        if ($(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle3,.sidenav-toggle4").show()) {
            $(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle3,.sidenav-toggle4").hide()
            $("#minus-toggle,#minus-toggle1,#minus-toggle3,#minus-toggle4").hide()
            $("#plus-toggle,#plus-toggle1,#plus-toggle3,#plus-toggle4").show()
        }
    })
    $("#minus-toggle2").click(function(){
        $(".sidenav-toggle2").hide()
        $("#minus-toggle2").hide()
        $("#plus-toggle2").show()
    })

    //Music toggle
    $("#minus-toggle3").hide()
    $("#plus-toggle3").click(function() {
        $(".sidenav-toggle3").show()
        $("#plus-toggle3").hide()
        $("#minus-toggle3").show()
        if ($(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle4").show()) {
            $(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle4").hide()
            $("#minus-toggle,#minus-toggle1,#minus-toggle2,#minus-toggle4").hide()
            $("#plus-toggle,#plus-toggle1,#plus-toggle2,#plus-toggle4").show()
        }
    })
    $("#minus-toggle3").click(function(){
        $(".sidenav-toggle3").hide()
        $("#minus-toggle3").hide()
        $("#plus-toggle3").show()
    })

    //Motor toggle
    $("#minus-toggle4").hide()
    $("#plus-toggle4").click(function() {
        $(".sidenav-toggle4").show()
        $("#plus-toggle4").hide()
        $("#minus-toggle4").show()
        if ($(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle3").show()) {
            $(".sidenav-toggle,.sidenav-toggle1,.sidenav-toggle2,.sidenav-toggle3").hide()
            $("#minus-toggle,#minus-toggle1,#minus-toggle2,#minus-toggle3").hide()
            $("#plus-toggle,#plus-toggle1,#plus-toggle2,#plus-toggle3").show()
        }
    })
    $("#minus-toggle4").click(function(){
        $(".sidenav-toggle4").hide()
        $("#minus-toggle4").hide()
        $("#plus-toggle4").show()
    })
})

function openNav() {
    if (window.matchMedia("(max-width: 992px)").matches) {
        document.getElementById("content").style.display = "none";
        document.getElementById("mySideNav").style.position = "fixed";
        document.getElementById("mySideNav").style.right = "0px";
        document.getElementById("wrapper").style.right = "177px";
        document.getElementById("wrapper").style.opacity = 0.8;
    }
}

//close sideNav when click on out the sideNav
$("#wrapper").click(function() {
    document.getElementById("wrapper").style.right = "0";
    document.getElementById("wrapper").style.opacity = 1;
    document.getElementById("mySideNav").style.position = "fixed";
    document.getElementById("mySideNav").style.right = "-177px";
})