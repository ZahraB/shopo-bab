$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $(".nav").addClass("animated slideInDown");
        $('.nav').css("box-shadow","0 0 3px rgba(0,0,0,.3)");
        $(".top-nav").addClass("animated slideInDown");
    } else {
        $(".nav").removeClass("animated slideInDown");
        $(".top-nav").removeClass("animated slideInDown");
    }
    if (scroll == 0) {
        $('.nav').css("box-shadow","none");
    }
    if (scroll <= 50) {
        $("#up").hide();
    } else {
        $("#up").show();
    }
})

$(document).ready(function(){
    var w = $(window).width();
    if (w <= 768) {
        $(".top-nav").show();
    }
})

// function myFunction(x) {
//     if (x.maches) {
//         $(".top-nav").show();
//     } else {
//         $(".top-nav").hide();
//     }
// }
// var x = window.matchMedia("(max-width: 768px)")
// myFunction(x)
// x.addListener(myFunction)