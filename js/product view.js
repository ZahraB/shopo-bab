$(document).ready(function() {
  $("#increment").click(function() {
    
  })
  $("#decrement").click(function() {
    
  })
})

function openTab() {
  $("#tab1").css("border-bottom","none");
  $("#tab2").css("border-bottom","none");
  $("#tab3").css("border-bottom","none");
  $("#tab4").css("border-bottom","2px solid #dc9831");
  if ($("#comment-btn, .review, #moreInfo, #reviews, .sizeChart").show()) {
    $("#comment-btn").hide();
    $("#reviews").hide();
    $(".review").hide();
    $("#moreInfo").hide();
    $(".sizeChart").hide();
    $(".product-custom-tab").show();
  }
}

function openSizetable() {
  $("#tab1").css("border-bottom","none");
  $("#tab2").css("border-bottom","none");
  $("#tab3").css("border-bottom","2px solid #dc9831");
  $("#tab4").css("border-bottom","none");
  if ($("#comment-btn, .review, #moreInfo, #reviews, .product-custom-tab").show()) {
    $("#comment-btn").hide();
    $("#reviews").hide();
    $(".review").hide();
    $("#moreInfo").hide();
    $(".sizeChart").show();
    $(".product-custom-tab").hide();
  }
}

function openReviews() {
  $("#tab1").css("border-bottom","none");
  $("#tab2").css("border-bottom","2px solid #dc9831");
  $("#tab3").css("border-bottom","none");
  $("#tab4").css("border-bottom","none");
  if ($("#comment-btn, .review, #moreInfo, .sizeChart, .product-custom-tab").show()) {
    $("#comment-btn").hide();
    $("#reviews").show();
    $(".review").hide();
    $("#moreInfo").hide();
    $(".sizeChart").hide();
    $(".product-custom-tab").hide();
  }
}
function writecomment() {
  $(".review").toggle();
  $("#comment-btn").toggle();
}

function openMoreinfo() {
  $("#tab1").css("border-bottom","2px solid #dc9831");
  $("#tab2").css("border-bottom","none");
  $("#tab3").css("border-bottom","none");
  $("#tab4").css("border-bottom","none");
  $("#moreInfo").show();
  if ($("#comment-btn, .review, #reviews, .sizeChart, .product-custom-tab").show()) {
    $("#comment-btn").hide();
    $(".review").hide();
    $("#reviews").hide();
    $(".sizeChart").hide();
    $(".product-custom-tab").hide();
    
  }
}

$('.owl-one').owlCarousel({
  loop:false,
  margin:10,
  nav:true,
  rtl: true,
  responsive:{
      0:{
          items:2
      },
      359:{
          items:2
      },
      767:{
          items:4
      },
      1199:{
          items:4
      }
  }
})